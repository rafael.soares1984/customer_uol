package com.br.customer;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.customer.entity.Customer;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@AutoConfigureWebMvc
public class  CustomerApplicationTests {

    @LocalServerPort
	 int randomServerPort;

	@Autowired
	private TestRestTemplate restTemplate;   
	
	private long id = 0;

    @Test
    public void testAddCustomerSuccess() throws URISyntaxException
    {
        final String baseUrl = "http://localhost:"+randomServerPort+"/api/customer/";
        URI uri = new URI(baseUrl);
        Customer customer = new Customer(null, "test",99);
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);     
 
        HttpEntity<Customer> request = new HttpEntity<>(customer, headers);
         
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
         
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testListCustomerSuccess () throws URISyntaxException
    {
        final String baseUrl = "http://localhost:"+randomServerPort+"/api/customer/";
        URI uri = new URI(baseUrl);
         
        ResponseEntity<String> result = this.restTemplate.getForEntity(uri, String.class);
         
        id = Long.parseLong(result.getBody().substring(result.getBody().indexOf("id")+4, result.getBody().indexOf(",")-1));
        
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().contains("test"));
    }

    @Test
    public void testListCustomerWithIDSuccess () throws URISyntaxException
    {
        
        final String baseUrl = "http://localhost:"+randomServerPort+"/api/customer/"+id;
        URI uri = new URI(baseUrl);
        
        ResponseEntity<String> result = this.restTemplate.getForEntity(uri, String.class);
         
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().contains("test"));
    }
}