## Test UOL

Application was developed by Rafael Soares.

  - **Service** [http://localhost:8080/api/customer]
  - **Swagger** [http://localhost:8080/swagger-ui.html]
  - **H2** [http://localhost:8080/h2]

# Functionalities of Application

  - **GET** [http://localhost:8080/api/customer] - List all customer
   
        GET /api/customer/ HTTP/1.1
        Host: localhost:8080
        cache-control: no-cache

  - **GET** [http://localhost:8080/api/customer/`{cusmoterId}`] - Get a specific customer

        GET /api/customer/1 HTTP/1.1
        Host: localhost:8080
        cache-control: no-cache

  - **POST** [http://localhost:8080/api/customer] Create a new customer
  
    Body ` { "name":"teste","age":99}`

        POST /api/customer HTTP/1.1
        Host: localhost:8080
        Content-Type: application/json
        cache-control: no-cache
        {
	        "name":"teste",
	        "age":99
        }------WebKitFormBoundary7MA4YWxkTrZu0gW--

  - **PUT** [http://localhost:8080/api/customer/`{customerId}`] Change a specific customer with customerId
  
    Body `{ "name":"rafael", "age":40 }`

        PUT /api/customer/100 HTTP/1.1
        Host: localhost:8080
        Content-Type: application/json
        cache-control: no-cache
        {
        	"name":"rafael",
        	"age":40
        }------WebKitFormBoundary7MA4YWxkTrZu0gW--
    
  - **DELETE** [http://localhost:8080/api/customer/{customerId}] Remove a custmer
        
        DELETE /api/customer/150 HTTP/1.1
        Host: localhost:8080
        Content-Type: application/json
        cache-control: no-cache

# Tools used for develop application

   - **Spring Boot**
        
        Spring boot was used, because it is easier to group the infrastructures configurations more easily
   - **Maven**
        
        Maven is an excellent package manager, as being the standard of the spring boot
   - **Apache Tomcat**
        
        Apache Tomcat as being the standard for the spring boot, was used
   - **H2 Database**
        
        Since no instructions on database usage were provided, we chose to use H2 Database, which does not require installation, as well as being portable
   - **Gitlab**
        
        Easier to use, I'm already used to it

# Application infrastructure

   - **Custmoer**

        Customer is very easy to a build package, test and run, enough install Java version 8, Apache Tomcat more recent version, Maven, Git

        - Run

          Enter a prompot of command and enter the instructions

          Fisrt clone repositóry in your desktop<br>
          `git clone https://gitlab.com/rafael.soares1984/customer_uol.git`      

          When clone is finish, enter on folder<br>
          `cd customer_uol`

          **Unix system**<br>
          ` ./mvnw clean install`

          **Windows**<br>
          `./mvnw.cmd clean install`

          After the command is finished, you can run other<br>
               `./mvnw spring-boot:run`

        - Build App

          Enter a promot of command and enter the instruction<br>
               `mvn clean install spring-boot:repackage` 
        
        - Apache Deploy
          
          Copy file `./target/customer.war` to a deply folder of server